﻿using System;
using System.Collections;
using System.Text;
using System.Configuration;
using System.Xml;

//https://docs.microsoft.com/en-us/previous-versions/2tw134k3(v=vs.140)
//https://stackoverflow.com/questions/3935331/how-to-implement-a-configurationsection-with-a-configurationelementcollection
namespace PayLock.EmailProcessor
{
    public class ServiceConfig : ConfigurationElement
    {
        public ServiceConfig() { }

        public ServiceConfig(string emailAddressKey, string host,int port, bool useSSL, string userName, string password, string postUrl, string bodyKey, string subjectKey,string fileFilter,string origin,string originKey, string posturl2, string subjectkey2)
        {
            Port = port;
            EmailAddressKey = emailAddressKey;
            Host = host;
            Port = port;
            UseSSL = useSSL;
            UserName = userName;
            Password = password;
            PostUrl = postUrl;
            BodyKey = bodyKey;
            SubjectKey = subjectKey;
            FileFilter = fileFilter;
            Origin = origin;
            OriginKey = originKey;
            PostUrl2 = posturl2;
            SubjectKey2 = subjectkey2;
        }

        [ConfigurationProperty("OriginKey", DefaultValue = "external", IsRequired = true, IsKey = false)]
        public string OriginKey
        {
            get { return (string)this["OriginKey"]; }
            set { this["OriginKey"] = value; }
        }
        [ConfigurationProperty("Origin", DefaultValue = "origin", IsRequired = true, IsKey = false)]
        public string Origin
        {
            get { return (string)this["Origin"]; }
            set { this["Origin"] = value; }
        }

        [ConfigurationProperty("EmailAddressKey", DefaultValue = "email", IsRequired = true, IsKey = false)]
        public string EmailAddressKey
        {
            get { return (string)this["EmailAddressKey"]; }
            set { this["EmailAddressKey"] = value; }
        }

        [ConfigurationProperty("Host", DefaultValue = "outlook.office365.com", IsRequired = true, IsKey = false)]
        public string Host
        {
            get { return (string)this["Host"]; }
            set { this["Host"] = value; }
        }

        [ConfigurationProperty("Port", DefaultValue = 993, IsRequired = true, IsKey = false)]
        public int Port
        {
            get { return (int)this["Port"]; }
            set { this["Port"] = value; }
        }

        [ConfigurationProperty("UseSSL", DefaultValue = "true", IsRequired = true, IsKey = false)]
        public bool UseSSL
        {
            get { return (bool)this["UseSSL"]; }
            set { this["UseSSL"] = value; }
        }

        [ConfigurationProperty("UserName", DefaultValue = "", IsRequired = true, IsKey = false)]
        public string UserName
        {
            get { return (string)this["UserName"]; }
            set { this["UserName"] = value; }
        }

        [ConfigurationProperty("Password", DefaultValue = "", IsRequired = true, IsKey = false)]
        public string Password
        {
            get { return (string)this["Password"]; }
            set { this["Password"] = value; }
        }

        [ConfigurationProperty("PostUrl", DefaultValue = "", IsRequired = true, IsKey = false)]
        public string PostUrl
        {
            get { return (string)this["PostUrl"]; }
            set { this["PostUrl"] = value; }
        }

        [ConfigurationProperty("PostUrl2", DefaultValue = "", IsRequired = true, IsKey = false)]
        public string PostUrl2
        {
            get { return (string)this["PostUrl2"]; }
            set { this["PostUrl2"] = value; }
        }

        [ConfigurationProperty("BodyKey", DefaultValue = "body", IsRequired = true, IsKey = false)]
        public string BodyKey
        {
            get { return (string)this["BodyKey"]; }
            set { this["BodyKey"] = value; }
        }


        [ConfigurationProperty("SubjectKey", DefaultValue = "id", IsRequired = true, IsKey = false)]
        public string SubjectKey
        {
            get { return (string)this["SubjectKey"]; }
            set { this["SubjectKey"] = value; }
        }
        [ConfigurationProperty("SubjectKey2", DefaultValue = "id", IsRequired = true, IsKey = false)]
        public string SubjectKey2
        {
            get { return (string)this["SubjectKey2"]; }
            set { this["SubjectKey2"] = value; }
        }

        [ConfigurationProperty("FileFilter", DefaultValue = ".GIF|.JPG|.PNG", IsRequired = true, IsKey = false)]
        public string FileFilter
        {
            get { return  ((string)this["FileFilter"]).ToUpper(); }
            set { this["FileFilter"] = value; }
        }

    }


    public class ServiceCollection : ConfigurationElementCollection
    {
        public ServiceCollection()
        {
            Console.WriteLine("ServiceCollection Constructor");
        }

        public ServiceConfig this[int index]
        {
            get { return (ServiceConfig)BaseGet(index); }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        public void Add(ServiceConfig serviceConfig)
        {
            BaseAdd(serviceConfig);
        }

        public void Clear()
        {
            BaseClear();
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ServiceConfig();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ServiceConfig)element).UserName;
        }

        public void Remove(ServiceConfig serviceConfig)
        {
            BaseRemove(serviceConfig.Port);
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }
    }

    public class ServiceConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("Services", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(ServiceCollection),
            AddItemName = "add",
            ClearItemsName = "clear",
            RemoveItemName = "remove")]
        public ServiceCollection Services
        {
            get
            {
                return (ServiceCollection)base["Services"];
            }
        }
    }


}