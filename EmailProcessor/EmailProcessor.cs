﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using AE.Net.Mail;
using log4net;
using Loggly;
using Attachment = AE.Net.Mail.Attachment;
using MailMessage = AE.Net.Mail.MailMessage;


namespace PayLock.EmailProcessor
{
    public class EmailProcessor
    {
        private ManualResetEvent eventPause;
        private ServiceConfigurationSection serviceConfigSection;
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private int sleepInterval;
        private string confirmationMessage;
        private string smtpAddress;
        private string fromAddress;
        public EmailProcessor()
        {
            eventPause = new ManualResetEvent(false);
            serviceConfigSection = ConfigurationManager.GetSection("ServicesSection") as ServiceConfigurationSection;
            sleepInterval = (ConfigurationManager.AppSettings["SleepIntervalMinutes"] == null) || (ConfigurationManager.AppSettings["SleepIntervalMinutes"].Trim() == string.Empty)
                ? 5 : Convert.ToInt32(ConfigurationManager.AppSettings["SleepIntervalMinutes"]);
            confirmationMessage = (ConfigurationManager.AppSettings["ConfirmationMessage"] == null) || (ConfigurationManager.AppSettings["ConfirmationMessage"].Trim() == string.Empty)
                ? "Dear Motorist, We have received the message you sent.\n Thank you, \n PayLock" : ConfigurationManager.AppSettings["ConfirmationMessage"];
            smtpAddress = ConfigurationManager.AppSettings["SmtpAddress"];
            fromAddress = ConfigurationManager.AppSettings["FromAddress"];
        }
        public void Start()
        {
            while (true)
            {
                foreach (ServiceConfig serviceConfig in serviceConfigSection.Services)
                {
                    try
                    {
                        ProcessMessages(serviceConfig);
                    }
                    catch (Exception ex)
                    {
                        _log.PublishError(string.Format("Error processing messages from:{0}. The error is:{1}", serviceConfig.UserName, ex.Message));
                    }
                }
                
                eventPause.WaitOne(sleepInterval * 60 *1000, false);
            }
        }

        public void RunOnce()
        {

            foreach (ServiceConfig serviceConfig in serviceConfigSection.Services)
                {
                    try
                    {
                        ProcessMessages(serviceConfig);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(string.Format("Error processing messages from:{0}. The error is:{1}", serviceConfig.UserName, ex.Message));
                        _log.PublishError(string.Format("Error processing messages from:{0}. The error is:{1}", serviceConfig.UserName, ex.Message));
                    }
                }

        }

        private void ProcessMessages(ServiceConfig serviceConfig)
        {
            ImapClient iclient = new ImapClient(serviceConfig.Host, serviceConfig.UserName, serviceConfig.Password,
                AuthMethods.Login, serviceConfig.Port, serviceConfig.UseSSL);

            iclient.SelectMailbox("INBOX");
            int numMessages = iclient.GetMessageCount();
            for (int i = 1; i <= numMessages; i++)
            {
                try
                {
                    // Get only message header
                    MailMessage msg = iclient.GetMessage(i - 1, true);

                    if (msg == null)
                        continue;

                    // Skip over deleted messages.
                    if ((msg.Flags & Flags.Deleted) == Flags.Deleted)
                        continue;

                    // Message message header AND BODY.
                    msg = iclient.GetMessage(i - 1, false);
                    SendDataToEndPoints(serviceConfig, FilterAttachments(msg.Attachments, serviceConfig.FileFilter),msg);


                    iclient.DeleteMessage(msg);
                    iclient.Expunge();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(string.Format(
                        "Error processing specific message number: {0} from:{1}. The error is:{2}", i,
                        serviceConfig.UserName, ex.Message));
                    _log.PublishError(string.Format("Error processing specific message number: {0} from:{1}. The error is:{2}", i,serviceConfig.UserName, ex.Message));
                }

            }
        }



        private ICollection<Attachment> FilterAttachments(ICollection<Attachment> attachments,string filter)
        {
            var  filteredAttachments = new List<Attachment>();
            string[] filters = filter.Split(',');
            foreach (var attachment in attachments)
            {
                var extensionType = Path.GetExtension(attachment.Filename)?.ToUpper().Trim();
                if(filters.Contains(extensionType))
                //if ((extensionType == ".JPG") || (extensionType == ".PNG") || (extensionType == ".PDF") || extensionType == ".GIF")
                    filteredAttachments.Add(attachment);
            }

            return filteredAttachments;
        }

        private StreamContent CreateFileContent(Stream stream, string fileName, string contentType)
        {
            var fileContent = new StreamContent(stream);
            fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
            {
                Name = "\"files\"",
                FileName = "\"" + fileName + "\""
            }; // the extra quotes are key here
            fileContent.Headers.ContentType = new MediaTypeHeaderValue(contentType);
            return fileContent;
        }

        private void SendDataToEndPoints(ServiceConfig serviceConfig, ICollection<Attachment> attachments,MailMessage msg)
        {
            SendDataToEndPoint(serviceConfig.PostUrl,attachments, msg,
                serviceConfig.SubjectKey, msg.Subject,
                serviceConfig.BodyKey, msg.Body, serviceConfig.EmailAddressKey, msg.From.Address, serviceConfig.OriginKey, serviceConfig.Origin);

            SendDataToEndPoint(serviceConfig.PostUrl2, attachments, msg,
                serviceConfig.SubjectKey2, msg.Subject,
                serviceConfig.BodyKey, msg.Body, serviceConfig.EmailAddressKey, msg.From.Address, serviceConfig.OriginKey, serviceConfig.Origin);

        }


        private void SendDataToEndPoint(string baseUrl,ICollection<Attachment> attachments, MailMessage message, params string[] urlParams)
        {
            if (string.IsNullOrEmpty(baseUrl))
                return;
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var content = new MultipartFormDataContent();
                    //attachments
                    foreach (var attachment in attachments)
                    {
                        var extensionType = Path.GetExtension(attachment.Filename).ToUpper();
                        byte[] image = attachment.GetData();
                        content.Add(CreateFileContent(new MemoryStream(image), attachment.Filename,
                            attachment.ContentType));
                    }

                    //Form Data
                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                    for (int i = 0; i < urlParams.Length; i = i + 2)
                    {
                        if(!string.IsNullOrEmpty(urlParams[i])) 
                            parameters[urlParams[i]] = urlParams[i + 1];
                    }

                    var formContent = new System.Net.Http.FormUrlEncodedContent(parameters);
                    content.Add(formContent, "postparams");

                    //post the data
                    var responseContent = httpClient.PostAsync(CreateUrl(baseUrl, urlParams), content).Result.Content
                        .ReadAsStringAsync().Result;
                    if (responseContent == "{\"success\":true}")
                    {
                        try
                        {
                            //send confirmation email
                            var client = new SmtpClient(smtpAddress,25);
                            client.Send(fromAddress, message.From.Address, "RE:" + message.Subject + " -Received",
                                confirmationMessage);
                            Console.WriteLine("Sent email to " + message.From.Address + " using smtp " + smtpAddress);
                        }
                        catch (Exception ex)
                        {
                            _log.PublishDebug("Failed to send confirmation email:" + ex.Message);
                            Console.WriteLine("Failed to send confirmation email:" + ex.Message);
                        }
                    }
                    _log.PublishDebug("Response from Server:" + responseContent);
                    Console.WriteLine("Response from Server:" + responseContent);
                }
            }
            catch (Exception ex)
            {
                _log.PublishError(string.Format("Error in SendDataToEndPoint to {0} . The error is:{1}", baseUrl, ex.Message));
                Console.WriteLine(string.Format("Error in SendDataToEndPoint to {0} . The error is:{1}", baseUrl, ex.Message));
                throw ex;
            }


        }

        private string CreateUrl(string baseUrl, params string[] urlParams)
        {
            var url = baseUrl.Split('?');
            var collection = HttpUtility.ParseQueryString(url.Length >1 ? url[1] : string.Empty);
            for(int i=0;i< urlParams.Length; i = i+2) 
            {
                if (!string.IsNullOrEmpty(urlParams[i]))
                    collection[urlParams[i]] = urlParams[i+1];
            }
            var builder = new UriBuilder(url[0]) { Query = collection.ToString() };
            _log.PublishDebug("Email Message sent to URI:" + builder.Uri.AbsoluteUri);
           return builder.Uri.AbsoluteUri;
        }
    }
}

