﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayLock.EmailListenerCmdApp
{
    class Program
    {
        static void Main(string[] args)
        {
            EmailProcessor.EmailProcessor processor = new EmailProcessor.EmailProcessor();
            processor.RunOnce();
        }
    }
}
