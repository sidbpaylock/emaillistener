﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;


namespace PayLock.EmailListener
{
    public partial class Service1 : ServiceBase
    {
        Timer timer = new Timer(); // name space(using System.Timers;) 

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {

            //timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            //timer.Interval = 5000; //number in milisecinds  
            //timer.Enabled = true;
            EmailProcessor.EmailProcessor processor = new EmailProcessor.EmailProcessor();
            processor.Start();

        }

        private void OnElapsedTime(object sender, ElapsedEventArgs e)
        {
            
        }

        protected override void OnStop()
        {
            
        }
    }
}
